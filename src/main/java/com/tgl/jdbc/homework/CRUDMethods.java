package com.tgl.jdbc.homework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

public class CRUDMethods {
	final String url = "jdbc:mysql://localhost:3306/jdbc?serverTimezone=UTC";
	final String user = "root";
	final String password = "woo90603";

	public void crudMethod(Method method, int id, Employee employee, String data1, String data2,Method method2, Method condition,Method sort) {
		
		switch (method) {
		case CREATE:
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection(url, user, password);
				Date date = new Date();
				Timestamp timestamp = new Timestamp(date.getTime());
				PreparedStatement stmt = con.prepareStatement(
						"insert into employee (NAME,ENNAME,TALL,WEIGHT,PHONE,EMAIL,BMI,createdAt) values(?,?,?,?,?,?,?,?)");
				stmt.setString(1, employee.getName());
				stmt.setString(2, employee.getEnName());
				stmt.setInt(3, employee.getTall());
				stmt.setInt(4, employee.getWeight());
				stmt.setString(5, employee.getPhone());
				stmt.setString(6, employee.getEmail());
				stmt.setFloat(7, employee.getBmi());
				stmt.setTimestamp(8, timestamp);
				stmt.executeUpdate();
				con.close();
				System.out.println("success Create!");
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			break;
		case DELETE:
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection(url, user, password);
				PreparedStatement stmt = con.prepareStatement(
						"DELETE FROM employee WHERE id = ?");
				stmt.setInt(1, id);
				stmt.executeUpdate();
				con.close();
				System.out.println("success Delete");
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			break;
		case UPDATE:
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection(url, user, password);
				Date date = new Date();
				Timestamp timestamp = new Timestamp(date.getTime());
				PreparedStatement stmt = con.prepareStatement(
						"UPDATE employee SET NAME=?, ENNAME=?, TALL=?, WEIGHT=?, PHONE=?, EMAIL=?, BMI=?, updatedAt = ? WHERE id = ? ");
				stmt.setString(1, employee.getName());
				stmt.setString(2, employee.getEnName());
				stmt.setInt(3, employee.getTall());
				stmt.setInt(4, employee.getWeight());
				stmt.setString(5, employee.getPhone());
				stmt.setString(6, employee.getEmail());
				stmt.setFloat(7, employee.getBmi());
				stmt.setTimestamp(8, timestamp);
				stmt.setInt(9, employee.getId());
				stmt.executeUpdate();
				con.close();
				System.out.println("success Update");
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			break;
		case READ:
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection(url, user, password);
				PreparedStatement stmt = null;
				
				if(method2.toString().equals("AND") &&  sort.toString().equals("ASC")) {
					switch(condition) {
					case ID:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY ID ASC");
						break;
					case ENGLISHNAME:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY ENNAME ASC");
						break;
					case TALL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY TALL ASC");
						break;
					case WEIGHT:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY WEIGHT ASC");
						break;
					case EMAIL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY EMAIL ASC");
						break;
					case BMI:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY BMI ASC");
						break;
					case PHONE:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY PHONE ASC");
						break;
					default:
						System.out.println("請輸入正確查詢條件");
						break;
					}
				}else if(method2.toString().equals("AND") &&  sort.toString().equals("DESC")){
					switch(condition) {
					case ID:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY ID DESC");
						break;
					case ENGLISHNAME:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY ENNAME DESC");
						break;
					case TALL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY TALL DESC");
						break;
					case WEIGHT:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY WEIGHT DESC");
						break;
					case EMAIL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY EMAIL DESC");
						break;
					case BMI:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY BMI DESC");
						break;
					case PHONE:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? AND WEIGHT > ? ORDER BY PHONE DESC");
						break;
					default:
						System.out.println("請輸入正確查詢條件");
						break;
					}
				}
				else if(method2.toString().equals("OR") && sort.toString().equals("ASC")) {
					switch(condition) {
					case ID:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY ID ASC");
						break;
					case ENGLISHNAME:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY ENNAME ASC");
						break;
					case TALL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY TALL ASC");
						break;
					case WEIGHT:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY WEIGHT ASC");
						break;
					case EMAIL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY EMAIL ASC");
						break;
					case BMI:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY BMI ASC");
						break;
					case PHONE:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY PHONE ASC");
						break;
					default:
						System.out.println("請輸入正確查詢條件");
						break;
					}
				}
				else if(method2.toString().equals("OR") &&  sort.toString().equals("DESC")){
					switch(condition) {
					case ID:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY ID DESC");
						break;
					case ENGLISHNAME:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY ENNAME DESC");
						break;
					case TALL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY TALL DESC");
						break;
					case WEIGHT:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY WEIGHT DESC");
						break;
					case EMAIL:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY EMAIL DESC");
						break;
					case BMI:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY BMI DESC");
						break;
					case PHONE:
						stmt = con.prepareStatement(
								"SELECT * FROM employee WHERE TALL > ? OR WEIGHT > ? ORDER BY PHONE DESC");
						break;
					default:
						System.out.println("請輸入正確查詢條件");
						break;
					}
				}else {System.out.println("請輸入正確查詢條件");}
				stmt.setInt(1, Integer.parseInt(data1));
				stmt.setInt(2, Integer.parseInt(data2));
				ResultSet rs = stmt.executeQuery();
				while(rs.next()) {
					String newName = rs.getString(2).replace(rs.getString(2).charAt(1), 'X');
					System.out.println(rs.getInt(1) + newName + " "+rs.getString(3) + " " +rs.getInt(4) + " " +rs.getInt(5) + " "+
							rs.getString(6) + " "+ rs.getString(7) + " "+ rs.getFloat(8));
				}
				con.close();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
	}
}
