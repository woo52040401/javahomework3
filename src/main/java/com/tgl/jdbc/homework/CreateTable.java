package com.tgl.jdbc.homework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CreateTable {
	public void createTable() {
		final String url = "jdbc:mysql://localhost:3306/jdbc?serverTimezone=UTC";
		final String user = "root";
		final String password = "woo90603";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(url, user, password);
			
			PreparedStatement stmt = con.prepareStatement(
					"CREATE TABLE employee" + "(ID INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(ID), NAME VARCHAR(50) NOT NULL, ENNAME VARCHAR(50) NOT NULL, TALL INT NOT NULL, WEIGHT INT NOT NULL, PHONE VARCHAR(50) NOT NULL , EMAIL VARCHAR(50) NOT NULL, BMI FLOAT NOT NULL, createdAt DATETIME, updatedAt DATETIME)");
			stmt.executeUpdate();
			System.out.println("Success create Table");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
}
