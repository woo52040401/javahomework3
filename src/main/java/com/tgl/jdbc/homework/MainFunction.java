package com.tgl.jdbc.homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainFunction {

	public static void main(String[] args) {
		try (BufferedReader read = new BufferedReader(new FileReader("D://名單.txt"))) {
			String str;
			String[] strArray;
			CRUDMethods crudMethods = new CRUDMethods();
			Employee employee = new Employee();

			CreateTable createTable = new CreateTable();
			// createTable.createTable();
			while ((str = read.readLine()) != null) {
				strArray = str.split("\\s+");
				float bmi = (float) (Integer.parseInt(strArray[1])
						/ Math.pow((Integer.parseInt(strArray[0]) * 0.01), 2));
				employee.setName(strArray[3]);
				employee.setEnName(strArray[2]);
				employee.setTall(Integer.parseInt(strArray[0]));
				employee.setWeight(Integer.parseInt(strArray[1]));
				employee.setPhone(strArray[4]);
				employee.setEmail(strArray[5]);
				employee.setBmi(bmi);
				// crudMethods.crudMethod(Method.CREATE, 0, employee, "", "",null,null,null);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		MainFunction main = new MainFunction();
		// Create
		// main.create();

		// Read
		// main.read(Method.AND, Method.BMI, Method.DESC);
		// main.read(Method.OR,Method.TALL, Method.ASC);

		// Update
		// main.update();

		// Delete
		//main.delete();

	}

	public void create() {
		Employee employee = new Employee();
		Employee employee2 = new Employee();
		CRUDMethods crudMethods = new CRUDMethods();
		List<Employee> lists = new ArrayList<Employee>();
		employee.setName("李");
		employee.setEnName("Yan");
		employee.setTall(175);
		employee.setWeight(62);
		employee.setPhone("2535");
		employee.setEmail("jaylo@transglobe.com.tw");
		employee.setBmi((float) (employee.getWeight() / Math.pow(employee.getTall() * 0.01, 2)));
		lists.add(employee);
		employee2.setName("王");
		employee2.setEnName("Wang");
		employee2.setTall(170);
		employee2.setWeight(58);
		employee2.setPhone("1234");
		employee2.setEmail("efeflr@transglobe.com.tw");
		employee2.setBmi((float) (employee.getWeight() / Math.pow(employee.getTall() * 0.01, 2)));
		lists.add(employee2);
		for (Employee list : lists) {
			crudMethods.crudMethod(Method.CREATE, 0, list, "", "", null, null, null);
		}
	}

	public void read(Method method, Method condition, Method sort) {
		CRUDMethods crudMethods = new CRUDMethods();
		String employeeTall = "175";
		String employeeWeight = "75";

		switch (method) {
		case AND:
			crudMethods.crudMethod(Method.READ, 0, null, employeeTall, employeeWeight, method, condition, sort);
			break;
		case OR:
			crudMethods.crudMethod(Method.READ, 0, null, employeeTall, employeeWeight, method, condition, sort);
			break;
		default:
			break;
		}
	}

	public void update() {
		final String url = "jdbc:mysql://localhost:3306/jdbc?serverTimezone=UTC";
		final String user = "root";
		final String password = "woo90603";
		CRUDMethods crudMethods = new CRUDMethods();
		List<Employee> lists = new ArrayList<Employee>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(url, user, password);
			Employee employee = new Employee();
			Employee employee2 = new Employee();
			int employeeId = 2;
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM employee WHERE id = ?");
			stmt.setInt(1, employeeId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				employee.setId(employeeId);
				employee.setName("企鵝");
				employee.setEnName("Tom");
				employee.setTall(rs.getInt(4));
				employee.setWeight(rs.getInt(5));
				employee.setPhone(rs.getString(6));
				employee.setEmail(rs.getString(7));
				employee.setBmi(rs.getFloat(8));
				lists.add(employee);
			}
			employeeId = 1;
			PreparedStatement stmt2 = con.prepareStatement("SELECT * FROM employee WHERE id = ?");
			stmt2.setInt(1, employeeId);
			ResultSet rs2 = stmt2.executeQuery();
			while (rs2.next()) {
				employee2.setId(employeeId);
				employee2.setName(rs2.getString(2));
				employee2.setEnName(rs2.getString(3));
				employee2.setTall(rs2.getInt(4));
				employee2.setWeight(rs2.getInt(5));
				employee2.setPhone("0215");
				employee2.setEmail("WGWEG@transglobe.com.tw");
				employee2.setBmi(rs2.getFloat(8));
				lists.add(employee2);
			}
			for (Employee list : lists) {
				crudMethods.crudMethod(Method.UPDATE, 0, list, "", "", null, null, null);
			}
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public void delete() {
		List<Integer> list = new ArrayList<Integer>();
		CRUDMethods crudMethods = new CRUDMethods();
		
		list.add(58);
		list.add(57);

		for (int i = 0; i < list.size(); i++) {
			crudMethods.crudMethod(Method.DELETE, list.get(i), null, "", "", null, null, null);
		}

	}
}
