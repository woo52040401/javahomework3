package com.tgl.jdbc.homework;

public class Employee {
	private int tall;
	private int weight;
	private String enName;
	private String name;
	private String phone;
	private String email;
	private float bmi;
	private int id;
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public float getBmi(){
		return bmi;
	}
	public void setBmi(float bmi) {
		this.bmi = bmi;
	}
	public int getTall() {
		return tall;
	}
	public void setTall(int tall) {
		if(tall < 75 || tall > 240) {
			System.out.println("錯誤的身高  請輸入正確值");
		}
		this.tall = tall;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		if(weight < 30 || weight > 150) {
			System.out.println("請輸入正確體重");
		}
		this.weight = weight;
	}
	public String getEnName() {
		return enName;
	}
	public void setEnName(String enName) {
		if(!enName.matches("[a-zA-Z]{1-30}-[a-zA-Z]{1-30}")) {
			System.out.println("請輸入正確英文姓名");
		}
		this.enName = enName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name.matches("[/u4e00-/u9fa5]+")) {
			System.out.println("請輸入正確姓名");
		}
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		if(!phone.matches("[0-9]+")) {
			System.out.println("請輸入正確電話");
		}
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Employee [id = " + id + " ,name=" + name + ", enName=" + enName + ", tall=" + tall + ", weight=" + weight
				+ ", phone=" + phone + ", email=" + email + ", bmi=" + bmi + "]";
	}
	
}
